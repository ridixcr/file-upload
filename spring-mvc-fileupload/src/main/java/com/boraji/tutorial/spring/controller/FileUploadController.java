package com.boraji.tutorial.spring.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {
   //https://www.boraji.com/spring-4-mvc-jquery-ajax-file-upload-example-with-progress-bar
   //http://hmkcode.com/java-servlet-jquery-file-upload/
   @GetMapping("/")
   public String fileUploadForm(Model model) {
      return "fileUploadForm";
   }

   @PostMapping("/fileUpload")
   public ResponseEntity<Object> fileUpload(@RequestParam("file") MultipartFile file)
         throws IOException {

      if (!file.getOriginalFilename().isEmpty()) {
         File f = new File(ensureDir("upload")+File.separator+file.getOriginalFilename());
         System.out.println(f.getAbsolutePath());
         BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(f));
         outputStream.write(file.getBytes());
         outputStream.flush();
         outputStream.close();
      }else{
         return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
      }
      
      return new ResponseEntity<>("File Uploaded Successfully.",HttpStatus.OK);
   }
   
   public String ensureDir(String path) {
        String outPath = System.getProperty("user.dir") +File.separator+ path;
        File f_out = new File(outPath);
        if (!f_out.exists()) {
            f_out.mkdirs();
        }
        return f_out.getAbsolutePath();
    }
}
